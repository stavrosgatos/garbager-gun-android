package com.stavrosgatos.run.garbager;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.stavrosgatos.run.garbager.assets.Background;
import com.stavrosgatos.run.garbager.assets.Can;
import com.stavrosgatos.run.garbager.assets.GameOver;
import com.stavrosgatos.run.garbager.assets.Hero;
import com.stavrosgatos.run.garbager.assets.TrashCan;
import com.stavrosgatos.run.garbager.common.constants.Constants;
import com.stavrosgatos.run.garbager.common.utils.Utils;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Intersector;

import java.util.concurrent.TimeUnit;

/**
 * The class GarbageCollectorRun.
 * Created by stike92 on 4/22/2018.
 */
class GarbagerRun extends ApplicationAdapter {

	private Background background;
	private Hero hero;
	private TrashCan trashCan;
	private Can[] can;
	private Can[] emptyCan;
	private GameOver gameOver;
	private Utils utils;
	private SpriteBatch batch;
	private Rectangle trashCanRectangle;
	private Rectangle heroRectangle;
	private Rectangle[] emptyCanRectangle;
	private BitmapFont bitmapFont;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void create () {

        background = new Background();
        gameOver = new GameOver();
        hero = new Hero();
        trashCan  = new TrashCan();
		utils = new Utils();
		batch = new SpriteBatch();
		bitmapFont = new BitmapFont();
		bitmapFont.getData().scale(5);
		bitmapFont.setColor(Color.WHITE);
		utils.setGameJustStarted(true);
		utils.setGameOver(false);
		utils.setHeroState(0);
		utils.setScore(0);
		utils.setTouchedTimes(0);
		utils.setEmptyCans(Constants.EMPTY_CANS - 1);
        heroRectangle = new Rectangle();
        trashCanRectangle = new Rectangle();
        emptyCanRectangle = new Rectangle[Constants.EMPTY_CANS];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void render () {

		if (!utils.isGameOver()) {
            start();
            createTrashCans();
            createCans();
            countJumpedTrashCans();
            // Make hero run.
            utils.changeHeroState(utils.getHeroState());
            jump();
            drawGraphics();
			detectCollision();
		} else {
		    drawGameOverGrahics();
			if(Gdx.input.justTouched()) {
                restartGame();
			}
		}
		//delay();
	}

	public void start() {
        // If the game just started or restarted, initialize the objects.
        if(utils.isGameJustStarted()) {
            hero = new Hero();
            hero.setPositionY(background.getStreet1().getHeight());
            trashCan  = new TrashCan();
            trashCan.setPositionY(background.getStreet1().getHeight());
            emptyCan = new Can[Constants.EMPTY_CANS];
            for (int i = 0; i < utils.getEmptyCans();  i++) {
                emptyCan[i] = new Can();
            }
            emptyCan[0].setPositionX(Gdx.graphics.getWidth() + utils.getDistance());
            for (int i = 1; i < utils.getEmptyCans();  i++) {
                emptyCan[i].setPositionX(emptyCan[i - 1].getPositionX() + Constants.MIN_CANS_DISTANCE);
            }
            utils.setTouchedTimes(0);
            utils.setGameJustStarted(false);
        }
    }

    public  void createTrashCans() {
        // Change objects' position according to the speed.
        trashCan.setPositionX( trashCan.getPositionX() - utils.getSpeed(utils.getScore()) );
        for (int i = 0; i < emptyCan.length - 1; i++) {
            emptyCan[i].setPositionX( emptyCan[i].getPositionX() - utils.getSpeed(utils.getScore()) );
        }

        // If a trash can is disappeared, create a new one.
        if (trashCan.getPositionX() + trashCan.getTrashCan().getWidth() < 0) {
            trashCan = new TrashCan();
            trashCan.setPositionY(background.getStreet1().getHeight());
        }
    }

    public void createCans() {
        // If empty cans are disappeared, create some new.
        utils.setVisibleEmptyCans(utils.getEmptyCans());
        for (int i = 0; i < utils.getEmptyCans(); i++) {
            if ( emptyCan[i].getPositionX() +  emptyCan[i].getEmptyCan().getWidth() < 0 ) {
                utils.setVisibleEmptyCans(utils.getVisibleEmptyCans() - 1);
            }
        }

        if (utils.getVisibleEmptyCans() == 0) {
            emptyCan[0] = new Can();
            emptyCan[0].setPositionX(Gdx.graphics.getWidth() + utils.getDistance());
            for (int i = 1; i < utils.getEmptyCans();  i++) {
                emptyCan[i].setPositionX(emptyCan[i - 1].getPositionX() + Constants.MIN_CANS_DISTANCE);
            }
            utils.setVisibleEmptyCans(utils.getVisibleEmptyCans() + 1);
        }
    }

    /**
     *  Make hero to jump on touch if he is on the ground.
     */
    public void jump() {
        if (Gdx.input.justTouched() && hero.getPositionY() <= background.getStreet1().getHeight()) {
            hero.setJumping(true);
            hero.setPositionY( hero.getPositionY() + Constants.GRAVITY * 4 );
            utils.setHeroState(Constants.HERO_STATES - 2);
        } else if (hero.isJumping() && hero.getPositionY() < background.getStreet1().getHeight() + Constants.JUMP_HEIGHT) {
            hero.setPositionY( hero.getPositionY() + Constants.GRAVITY * 2 );
            utils.setHeroState(Constants.HERO_STATES - 2);
            if(Gdx.input.justTouched() && utils.getTouchedTimes() < 2) {
                emptyCan[utils.getEmptyCans()] = new Can();
                emptyCan[utils.getEmptyCans()].setPositionX(hero.getPositionX(utils.getHeroState()));
                emptyCan[utils.getEmptyCans()].setPositionY(hero.getPositionY());
                utils.setTouchedTimes(2);
            }
        } else if (hero.getPositionY() > background.getStreet1().getHeight()) {
            hero.setPositionY( hero.getPositionY() - Constants.GRAVITY );
            hero.setPositionY( hero.getPositionY() - Constants.GRAVITY );
            if ( emptyCan[utils.getEmptyCans()] != null) {
                emptyCan[utils.getEmptyCans()].setPositionY(emptyCan[utils.getEmptyCans()].getPositionY() - Constants.GRAVITY);
            }
            hero.setJumping(false);
            utils.setHeroState(Constants.HERO_STATES - 2);
        } else {
            utils.setTouchedTimes(0);
        }
        if ( emptyCan[utils.getEmptyCans()] != null) {
            emptyCan[utils.getEmptyCans()].setPositionY(emptyCan[utils.getEmptyCans()].getPositionY() - Constants.GRAVITY);
        }
    }

    /**
     * If hero jumps over a trash can, increase the score by 1.
     */
    public void countJumpedTrashCans() {
        //
        if(trashCan.getPositionX()  < Gdx.graphics.getWidth() / 2
                && trashCan.getPositionX() >= Gdx.graphics.getWidth() / 2 - utils.getSpeed(utils.getScore())) {
            utils.setScore(utils.getScore() + 1);
        }
    }

    /**
     * Draw the graphics.
     */
    public void drawGraphics() {
        batch.begin();
        batch.draw(background.getSky(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.draw(background.getCity(), 0, background.getStreet1().getHeight());
        bitmapFont.draw(batch, String.valueOf(utils.getScore()),100, Gdx.graphics.getHeight() - 100);
        batch.draw(trashCan.getTrashCan(), trashCan.getPositionX(), trashCan.getPositionY());
        for (int i = 0; i < Constants.EMPTY_CANS; i++) {
            if (emptyCan[i] != null) {
                batch.draw(emptyCan[i].getEmptyCan(), emptyCan[i].getPositionX(), emptyCan[i].getPositionY());
            }
        }
        batch.draw(background.getStreet1(), 0, 0);
        batch.draw(hero.getHero(utils.getHeroState()), hero.getPositionX(utils.getHeroState()), hero.getPositionY());
        batch.end();
    }

    /**
     * Draw the graphics when the game is over.
     */
    public void drawGameOverGrahics() {
        batch.begin();
        utils.setHeroState(Constants.HERO_STATES - 1);
        hero.setPositionY(hero.getPositionY() + 2);
        hero.setPositionX(hero.getPositionX(utils.getHeroState()) +  2, utils.getHeroState());
        batch.draw(background.getSky(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.draw(background.getCity(), 0, background.getStreet1().getHeight());
        batch.draw(trashCan.getTrashCan(), trashCan.getPositionX(), trashCan.getPositionY());
        batch.draw(hero.getHero(utils.getHeroState()), hero.getPositionX(utils.getHeroState()), hero.getPositionY());
        for (int i = 0; i < Constants.EMPTY_CANS; i++) {
            if (emptyCan[i] != null) {
                batch.draw(emptyCan[i].getEmptyCan(), emptyCan[i].getPositionX(), emptyCan[i].getPositionY());
            }
        }
        batch.draw(background.getStreet1(), 0, 0);
        batch.draw(gameOver.getGameOver(), Gdx.graphics.getWidth() / 2 - gameOver.getGameOver().getWidth() / 2, 400);
        bitmapFont.draw(batch, String.valueOf(utils.getScore()), 100, Gdx.graphics.getHeight() - 100);
        batch.end();
    }

    /**
     * Detect collision between hero and trash cans,
     * hero and cans, cans and trash cans.
     */
    public void detectCollision() {
        trashCanRectangle = new Rectangle(trashCan.getPositionX(), trashCan.getPositionY(),
                (trashCan.getTrashCan().getWidth() * 9) / 10, (trashCan.getTrashCan().getHeight() * 9) / 10);
        heroRectangle = new Rectangle(hero.getPositionX(utils.getHeroState()), hero.getPositionY(),
                hero.getHero(0).getWidth(), hero.getHero(0).getHeight());
        for (int i = 0; i < utils.getEmptyCans(); i++) {
            emptyCanRectangle[i] = new Rectangle(emptyCan[i].getPositionX(), emptyCan[i].getPositionY(),
                    emptyCan[i].getEmptyCan().getWidth(), emptyCan[i].getEmptyCan().getHeight());
            if (Intersector.overlaps(heroRectangle, emptyCanRectangle[i])) {
                utils.setScore(utils.getScore() + 2);
                emptyCan[i].setPositionX(0 - emptyCan[i].getEmptyCan().getWidth());
            }
        }
        if (emptyCan[utils.getEmptyCans()] != null) {
            emptyCanRectangle[utils.getEmptyCans()] = new Rectangle(emptyCan[utils.getEmptyCans()].getPositionX(),
                    emptyCan[utils.getEmptyCans()].getPositionY(), emptyCan[utils.getEmptyCans()].getEmptyCan().getWidth(),
                    emptyCan[utils.getEmptyCans()].getEmptyCan().getHeight());
            if (Intersector.overlaps(trashCanRectangle, emptyCanRectangle[utils.getEmptyCans()])) {
                emptyCan[utils.getEmptyCans()].setPositionX(0 - emptyCan[utils.getEmptyCans()].getEmptyCan().getWidth());
            }
        }
        if (Intersector.overlaps(heroRectangle, trashCanRectangle)) {
            utils.setGameOver(true);
        }
    }

    /**
     * Slow down the rendering.
     */
    public void delay() {
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Restart the game.
     */
    public void  restartGame() {
        utils.setGameOver(false);
        utils.setGameJustStarted(true);
        utils.setHeroState(0);
        utils.setScore(0);
    }

    /**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose () {
		batch.dispose();
		background.getSky().dispose();
	}

}
