package com.stavrosgatos.run.garbager.assets;

import com.badlogic.gdx.graphics.Texture;
import com.stavrosgatos.run.garbager.common.constants.Constants;

public class Background {

    private Texture sky;
    private Texture street1;
    private Texture street2;
    private Texture cloud1;
    private Texture cloud2;
    private Texture city;

    public Background() {
        this.sky = new Texture(Constants.SKY_SPRITE);
        this.street1 = new Texture(Constants.STREET_1_SPRITE);
        this.street2 = new Texture(Constants.STREET_2_SPRITE);;
        this.cloud1 = new Texture(Constants.CLOUD_1_SPRITE);
        this.cloud2 = new Texture(Constants.CLOUD_2_SPRITE);
        this.city = new Texture(Constants.CITY_SPRITE);
    }

    public Texture getSky() {
        return sky;
    }

    public void setSky(Texture sky) {
        this.sky = sky;
    }

    public Texture getStreet1() {
        return street1;
    }

    public void setStreet1(Texture street1) {
        this.street1 = street1;
    }

    public Texture getStreet2() {
        return street2;
    }

    public void setStreet2(Texture street2) {
        this.street2 = street2;
    }

    public Texture getCloud1() {
        return cloud1;
    }

    public void setCloud1(Texture cloud1) {
        this.cloud1 = cloud1;
    }

    public Texture getCloud2() {
        return cloud2;
    }

    public void setCloud2(Texture cloud2) {
        this.cloud2 = cloud2;
    }

    public Texture getCity() {
        return city;
    }

    public void setCity(Texture city) {
        this.city = city;
    }

}
