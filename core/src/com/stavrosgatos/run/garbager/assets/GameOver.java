package com.stavrosgatos.run.garbager.assets;

import com.badlogic.gdx.graphics.Texture;
import com.stavrosgatos.run.garbager.common.constants.Constants;

public class GameOver {

    private Texture gameOver;

    public GameOver() {
        this.gameOver = new Texture(Constants.GAME_OVER_SPRITE);
    }

    public Texture getGameOver() {
        return gameOver;
    }

    public void setGameOver(Texture gameOver) {
        this.gameOver = gameOver;
    }
}
