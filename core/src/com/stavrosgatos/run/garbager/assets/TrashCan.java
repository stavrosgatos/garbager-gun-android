package com.stavrosgatos.run.garbager.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.stavrosgatos.run.garbager.common.constants.Constants;
import com.stavrosgatos.run.garbager.common.utils.Utils;

public class TrashCan {

    private Texture trashCan;
    private int positionX;
    private int positionY;

    public TrashCan() {
        Utils utils = new Utils();
        this.trashCan = new Texture(Constants.TRASH_CAN_SPRITE);
        this.positionX = Gdx.graphics.getWidth() + utils.getDistance();
    }

    public Texture getTrashCan() {
        return trashCan;
    }

    public void setTrashCan(Texture trashCan) {
        this.trashCan = trashCan;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }
}
