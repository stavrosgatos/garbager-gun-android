package com.stavrosgatos.run.garbager.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.stavrosgatos.run.garbager.common.constants.Constants;
import com.stavrosgatos.run.garbager.common.utils.Utils;

public class Can {

    private Texture can;
    private Texture emptyCan;
    private int positionX;
    private int positionY;
    private static int distance = 0;

    public Can() {
        Utils utils = new Utils();
        this.distance += Constants.MIN_CANS_DISTANCE;
        this.can = new Texture(Constants.CAN_SPRITE);
        this.emptyCan = new Texture(Constants.EMPTY_CAN_SPRITE);
        this.positionY = utils.getCanPositionY();
    }

    public Texture getCan() {
        return can;
    }

    public void setCan(Texture can) {
        this.can = can;
    }

    public Texture getEmptyCan() {
        return emptyCan;
    }

    public void setEmptyCan(Texture can) {
        this.can = can;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

}
