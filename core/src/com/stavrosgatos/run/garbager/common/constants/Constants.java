package com.stavrosgatos.run.garbager.common.constants;

/**
 * The class Constants.
 * Created by stike92 on 4/22/2018.
 * Contains common constants.
 */
public class Constants {
    public static final String SKY_SPRITE = "sky.png";
    public static final String CLOUD_1_SPRITE = "cloud-1.png";
    public static final String CLOUD_2_SPRITE = "cloud-2.png";
    public static final String CITY_SPRITE = "city.png";
    public static final String STREET_1_SPRITE = "street-1.png";
    public static final String STREET_2_SPRITE = "street-2.png";;
    public static final String GAME_OVER_SPRITE = "game-over.png";
    public static final String TRASH_CAN_SPRITE = "trash-can-1.png";
    public static final String CAN_SPRITE = "can.png";
    public static final String EMPTY_CAN_SPRITE = "smasted-can-red.png";
    public static final String HERO_SPRITE = "hero-";
    public static final String HERO_SPRITE_EXTENSION = ".png";
    public static final int HERO_STATES = 5;
    public static final int MIN_CANS_DISTANCE = 80;
    public static final int JUMP_HEIGHT =700;
    public static final int GRAVITY = 8;
    public static final int INITIAL_SPEED = 16;
    public static final int EMPTY_CANS = 5;
    public static final int CANS = 1;
}
